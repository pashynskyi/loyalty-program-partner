<?php

namespace LoyaltyProgramPartner;

use LoyaltyProgramPartner\Entities\Client;
use LoyaltyProgramPartner\Entities\Order;

class API
{
    const REMOTE_SERVER = 'https://clients.gbarworld.com/api/v1/';

    private $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function bindClient(Client $client)
    {
        return self::send(Curl::METHOD_POST, 'client/associate', [], (array)$client);
    }
    
    public function getClientBonuses($client_id)
    {
        return self::send(Curl::METHOD_GET, 'client/balance', [], ['id' => $client_id]);
    }

    public function getTransactions($client_id)
    {
        return self::send(Curl::METHOD_GET, 'client/transactions', [], ['client_id' => $client_id]);
    }

    public function makeTransaction(Order $order)
    {
        return self::send(Curl::METHOD_POST, 'transaction', (array)$order);
    }

    public function deleteTransaction($id)
    {
        return self::send(Curl::METHOD_DELETE, 'transaction', [], ['id' => $id]);
    }

    private function send($method, $action, array $post = [], array $get = [])
    {
        $get = array_merge($get, ['api_token' => $this->token]);
        $response = Curl::send($method, self::REMOTE_SERVER . $action . '?' . http_build_query($get), $post);
        return json_decode($response);
    }
}
