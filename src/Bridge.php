<?php

namespace LoyaltyProgramPartner;

abstract class Bridge
{
    abstract public static function all($fromDate = null);

    abstract public static function get_by_order_id($id);

    public function getData()
    {
        $queryParams = $_GET;

        if(!isset($queryParams['action'])) {
            throw new \Exception('Undefined action');
        }

        $action = $queryParams['action'];
        unset($queryParams['action']);

        $bridge = new \ReflectionClass(static::class);
        $apiMethods = $bridge->getMethods(\ReflectionMethod::IS_STATIC && \ReflectionMethod::IS_PUBLIC);

        if(!in_array($action, array_column($apiMethods, 'name'))) {
            throw new \Exception('Invalid action!');
        }

        $actionMethod = new \ReflectionMethod(static::class, $action);
        $methodParams = array_column($actionMethod->getParameters(), 'name');

        $queryParams = (array)$queryParams;

        $params = array_map(function($param) use ($queryParams) {
            return isset($queryParams[$param]) ? $queryParams[$param] : null;
        }, $methodParams);

        $results = call_user_func_array([static::class, $action], $params);

//        echo '<pre>';
//        print_r($results);

        return $results;
    }
}
