<?php

namespace LoyaltyProgramPartner;

class Curl
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';

    public static function send($method, $url, array $post = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        $headers = [
            "accept: application/json",
            "content-type: application/json",
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        if(!empty($post)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post));
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 25);

        $data = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if($code == 200 && $data)
        {
            return $data;
        }
        return false;
    }
}
