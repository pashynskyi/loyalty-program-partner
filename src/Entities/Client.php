<?php

namespace LoyaltyProgramPartner\Entities;

class Client extends Entity
{
    public $name;
    public $email;
    public $phone;
    public $locale;

    public function __construct($id, $name, $email, $phone, $locale)
    {
        parent::__construct($id);

        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->locale = $locale;
    }
}
