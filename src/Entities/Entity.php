<?php

namespace LoyaltyProgramPartner\Entities;

abstract class Entity
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function fill(array $data = [])
    {
        foreach ($data as $key => $value) {
            if (property_exists(static::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}
