<?php

namespace LoyaltyProgramPartner\Entities;

class Order extends Entity
{
    public $amount;
    public $withdrawn_amount;
    public $description;
    public $transaction_at;
    public $client_id;
    public $products;

    /**
     * Order constructor.
     * @param mixed $id Internal order id
     * @param float $amount Internal order purchase price amount
     * @param float $withdrawn_amount Amount of bonuses withdrawn in current transaction
     * @param mixed $client_id Internal client id
     * @param string $description Internal order description that will be shown in transaction list
     * @param string $transaction_at DateTime string in YYYY-MM-DD HH:MM:SS format when transaction was confirmed (paid)
     * @param Product[] $products List of products associated with current transaction (order)
     * @param array[] $options
     */
    public function __construct(
        $id,
        $amount,
        $withdrawn_amount,
        $client_id,
        $description = null,
        $transaction_at = null,
        $products = [],
        $options = []
    )
    {
        parent::__construct($id);

        $this->amount = $amount;
        $this->withdrawn_amount = $withdrawn_amount;
        $this->client_id = $client_id;
        $this->transaction_at = $transaction_at;
        $this->description = $description;
        $this->products = $products;

        foreach($options as $key => $value) {
            $this->{$key} = $value;
        }
    }
}
