<?php

namespace LoyaltyProgramPartner\Entities;

class Product extends Entity
{
    const TYPE_PRODUCT = 'product';
    const TYPE_SERVICE = 'service';

    public $name;
    public $image;
    public $price;
    public $url;
    public $type;
    public $quantity;

    public function __construct($id, $quantity, $name, $price, $type, $image = null, $url = null)
    {
        parent::__construct($id);

        $this->name = $name;
        $this->quantity = $quantity;
        $this->image = $image;
        $this->price = $price;
        $this->url = $url;
        $this->type = $type;
    }
}
