<?php

namespace LoyaltyProgramPartner;

class Instance
{
    /**
     * @param $bridge Bridge
     * @param $token string
     * @param $is_json boolean
     * @return array
     */
    public static function serve($bridge, $token, $is_json = true)
    {
        if($_GET['token'] != $token) {
            throw new \Exception('Invalid token!');
        }

        $result = $bridge::getData();
        return $is_json ? json_encode($result) : $result;
    }
}
